function ObserveArray(arr, recursive) {
    recursive = recursive || false;
    var arrMethods = ['push', 'pop', 'shift', 'unshift', 'splice', 'sort', 'reverse'];
    var arrProps = {
        '_': {
            value: arr.slice(),
        }
    };

    // Подготовить объект-расширение массива
    for (var i = 0, c = arrMethods.length; i < c; i++) {
        arrProps[arrMethods[i]] = { value: preparePropValue(arrMethods[i]) };
    }

    // Назначить "расширение" массиву
    Object.defineProperties(arr, arrProps);

    // "Расширить" возможности элементов массива
    for (i in arr) {
        defineArrItemProp(arr, i);

        if (recursive && Array.isArray(arr[i])) {
            ObserveArray(arr[i], recursive);
        }
    }

    function preparePropValue(method) {
        return function () {
            var result = Array.prototype[method].apply(this._, arguments);
            // Array.prototype[method].apply(this, arguments);
            this.length = 0

            for (var i = 0, c = this._.length; i < c; i++) {
                // var descriptor = Object.getOwnPropertyDescriptor(this, i);
                // if (!('set' in descriptor)) {
                    defineArrItemProp(this, i)
                // }
            }

            return result;
        }
    }

    function defineArrItemProp(arr, i) {
        Object.defineProperty(arr, i, {
            get: function () {
                console.log('get', i, 'value: ' + this._[i]);
                return this._[i];
            },
            set: function (x) {
                console.log('set', i, 'old value: ' +  this._[i], 'new value: ' + x);
                this._[i] = x;
            },
            enumerable: true,
            configurable: true,
        });
    }
}
