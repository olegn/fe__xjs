/**
 * Определить тип переменной
 * @param {*} data
 */
function detectDataType(data) {
    var type;

    if (typeof data == 'string' || data instanceof String) {
        // is string
        type = 'string';
    } else if (typeof data == 'number') {
        // is number
        type = 'number';
    } else if ((typeof data == 'object' || data instanceof Object) && data.constructor == Object) {
        // is object like {}
        type = 'object';
    } else if ((typeof data == 'object' || data instanceof Object) && data.constructor == Array) {
        // is array like []
        type = 'array';
    } else if (typeof data == 'function' || data instanceof Function || (data instanceof Object && data.constructor == Function)) {
        // is function
        type = 'function';
    } else if (typeof data == 'object' && data === null) {
        // is null
        type = 'null';
    } else {
        type = typeof data;
    }

    return type;
}
