xjs.extend('object', function (obj) {
    return obj;
}).proto('values', function () {
    var obj = this
    var result;

    if ('values' in Object) {
        result = Object.values(obj);
    } else {
        var values = [];

        for (var key in obj) {
            values.push(obj[key]);
        }

        result = values;
    }

    return result;
}).proto('keys', function () {
    var obj = this
    var result;

    if ('values' in Object) {
        result = Object.keys(obj);
    } else {
        var keys = [];

        for (var key in obj) {
            keys.push(key);
        }

        result = keys;
    }

    return result;
});
