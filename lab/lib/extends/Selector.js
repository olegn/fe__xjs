xjs.extend('selector', function (selector, context) {
    /* this.selector =  */selector = selector || null;
    /* this.context =  */context = context || document;

    // nodeList = Array.prototype.slice.call(context.querySelectorAll(selector));
    var nodeList = context.querySelectorAll(selector);

    return nodeList;
}).proto('item', function (index) {
    var result;

    if (this instanceof Array || this instanceof NodeList) {
        // return this.splice(index, 1);
        return this[index];
    } else if (this instanceof Element) {
        result = this;
    }

    return result;
}).proto('first', function () {
    return this.item(0);
}).proto('last', function () {
    return this.item(this.length - 1)
}).proto('next', function () {
    var newNodeList = [];

    if (this instanceof Array || this instanceof NodeList) {
        for (var i = 0, c = this.length; i < c; i++) {
            newNodeList.push(this[i].nextElementSibling);
        }
    } else if (this instanceof Element) {
        newNodeList = this.nextElementSibling
        // newNodeList.push(this.nextElementSibling);
    }

    return newNodeList;
}).proto('prev', function () {
    var newNodeList = [];

    if (this instanceof Array || this instanceof NodeList) {
        for (var i = 0, c = this.length; i < c; i++) {
            newNodeList.push(this[i].previousElementSibling);
        }
    } else if (this instanceof Element) {
        newNodeList = this.previousElementSibling
        // newNodeList.push(this.nextElementSibling);
    }

    return newNodeList;
}).proto('style', function (name, value) {
    if (this instanceof Array || this instanceof NodeList) {
        for (var i = 0, c = this.length; i < c; i++) {
            this[i].style[name] = value;
        }
    } else if (this instanceof Element) {
        this.style[name] = value;
        // this.setAttribute('style', name + ': ' + value)
    }

    return this;
});
