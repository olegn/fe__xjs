(function (win) {
    var self = {};

    var extName;
    var extMethod;
    var methods = {};
    var res;

    var xjs = function () {
        return self;
    };

    /**
     * Метод для определения свойств объекта
     *
     * @param {Object} obj      - объект
     * @param {string} name     - имя свойства
     * @param {*} value         - значение
     */
    function _defineProperty(obj, name, value) {
        Object.defineProperty(obj, name, {
            enumerable: false,
            configurable: false,
            writable: false,
            value: value,
        });
    }

    function extend(name, method) {
        _defineProperty(this, name, method);
        return self.extend;
    }

    function extendArrayObject(arr, methods) {
        arr = arr || [];
        methods = methods || {};
        // console.log(arr.constructor)

        function xjsArray() {}

        if (arr.constructor === Array) {
            xjsArray.prototype = new Array();
        } else if (arr.constructor === Object) {
            xjsArray.prototype = new Object();
        }/*  else {

        } */

        if (arr.constructor === Array /* || arr.constructor === Object */) {
            for (var name in methods) {
                _defineProperty(xjsArray.prototype, name, methods[name]);
            }

            var arrayObject = new xjsArray();
        } else {
            for (var name in methods) {
                _defineProperty(arr, name, methods[name]);
            }

            var arrayObject = arr;
        }

        if (arr.constructor === Array) {
            for (var i = 0; i < arr.length; i++) {
                arrayObject.push(arr[i]);
            }
        } else if (arr.constructor === Object) {
        // } else {
            for (var i in arr) {
                arrayObject[i] = arr[i];
            }
        }

        return arrayObject;
    }

    _defineProperty(self, 'extend', function (name, method) {
        extName;
        extMethod;
        methods = {};
        res;

        extName = name;
        extMethod = method;

        return extend.call(self, name, function () {
            res = extendArrayObject(method.apply(method, arguments), methods);
            return res;
        });
    });

    _defineProperty(self.extend, 'proto', function (name, method) {
        // console.log(methods)
        methods[name] = function () {
            res = method.apply(res, arguments);
            res = extendArrayObject(res, methods);
            return res;
        }

        return extend.call(extMethod, name, methods[name]);
    });

    Object.defineProperty(win, 'xjs', {
        get: function () {
            return new (xjs)();
        },
        set: undefined,
    })
})(Function('return this')());
