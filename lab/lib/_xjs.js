var xjs = (function (win) {
    var self = {};
    var methods = {};
    self._array = [];

    /**
     * Определить метод для расширения библиотеки
     */
    _defineProperty(self, 'extend', function (name, method) {
        return extend.call(self, name, method);
    })

    /**
     * .
     */
    // _defineProperty(self, '_defineProperty', _defineProperty)


    /**
     * Метод для определения свойств объекта
     *
     * @param {Object} obj      - объект
     * @param {string} name     - имя свойства
     * @param {*} value         - значение
     */
    function _defineProperty(obj, name, value) {
        Object.defineProperty(obj, name, {
            enumerable: false,
            configurable: false,
            writable: false,
            value: value,
        });
    }

    /**
     * Метод для расширения библиотеки
     *
     * @param {string} name     - имя метода
     * @param {Function} method - функция метода
     */
    function extend0(name, method) {
        _defineProperty(this, name, function () {
            for (var key in method) {
                delete method[key];
            }
            return method.apply(method, arguments) || method;
        });

        _defineProperty(method, 'proto', function (_name, _method) {
            _defineProperty(method, _name, function () {
                return _method.apply(method, arguments) || method;
            });

            // extend.call(method, _name, function () {
            //     return _method.call(method, _name, _method);
            // });

            return method;
        });

        _defineProperty(method, '_', self);

        return method;
    }

    /**
     * Метод для расширения библиотеки
     *
     * @param {string} name     - имя метода
     * @param {Function} method - функция метода
     */
    function extend1(name, method) {
        if (!(name in methods)) {
            methods[name] = [];
        }

        _defineProperty(this, name, function () {
            for (var key in method) {
                delete method[key];
            }

            var result = method.apply(method, arguments) || method;

            // _defineProperty(result, 'proto', {});
            // console.log(result)
            // for (var i = 0, c = methods[name].length; i < c; i++) {
            //     _defineProperty(result, methods[name][i].name, methods[name][i].method);
            // }
            Object.defineProperties(result, methods[name]);
            // result.prototype = methods[name];

            return result;
        });
        _defineProperty(this[name], 'name', name);
        _defineProperty(this[name], 'methods', {});

        _defineProperty(method, 'proto', function (_name, _method) {
            // _defineProperty(self[name].methods, _name, _method);
            // methods[name].push({name: _name, method: _method});
            methods[name][_name] = {value: _method};
            // methods[name][_name] = _method;
            return method;
        });

        _defineProperty(method, '_', self);

        return method;
    }

    /**
     * Метод для расширения библиотеки
     *
     * @param {string} name     - имя метода
     * @param {Function} method - функция метода
     */
    function extend(name, method) {
        if (!(name in methods)) {
            methods[name] = {};
        }

        _defineProperty(this, name, function () {
            for (var key in method) {
                delete method[key];
            }

            var result = method.apply(method, arguments) || method;

            console.log(typeof result, result.constructor, methods[name])

            var resultType = typeof result;

            if (resultType == 'function' || resultType == 'object') {
                Object.defineProperties(result, methods[name]);
            } else if (resultType == 'array') {
                var _result = this._array

                for (var i = 0, c = result.length; i < c; i++) {
                    _result.push(result[i]);
                }

                result = _result;
            }

            console.log(result)

            return result;
        });
        // _defineProperty(this[name], 'name', name);
        // _defineProperty(this[name], 'methods', {});

        _defineProperty(method, 'proto', function (_name, _method) {
            _defineProperty(self._array, _name, _method);
            // methods[name].push({name: _name, method: _method});
            methods[name][_name] = {value: _method};
            return method;
        });

        _defineProperty(method, '_', self);

        return method;
    }

    return self;
})(Function('return this')());
