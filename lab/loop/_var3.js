var Loop = (function () {
    var _this_ = this;
    _this_.q = 123;

    return function (x) {
        _this_.q = x;
        return _this_;
    };
})();
