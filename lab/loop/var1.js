var Loop = function () {
    var _this_ = this;
    var _data = {};
    var _domMap = {};

    function prepareData(data) {
        for (var key in data) {
            defineDataProperties(key, data)
        }
    };

    function defineDataProperties(key, data) {
        _data[key] = data[key];
        var regex = new RegExp('{{\\s{0,}' + key + '\\s{0,}}}', 'gm');

        Object.defineProperty(_data, key, {
            get: function () {
                return {
                    regex: regex,
                    value: data[key],
                };
            },
            set: function (newVal) {
                data[key] = newVal;

                for (var i = 0, c = _domMap[_this_.componentName].length; i < c; i++) {
                    // for (var ii = 0, cc = _domMap[_this_.componentName][i].nodes.length; ii < cc; ii++) {
                        _domMap[_this_.componentName][i].props[key].nodeIndexes.forEach(function (val) {
                            _domMap[_this_.componentName][i].nodes[val].link.nodeValue = _domMap[_this_.componentName][i].nodes[val].blocks.map(function (val) {
                                return ((typeof val === 'string' || val instanceof String) ? val : _data[val.data].value);
                            }).join('');
                        });
                    // }
                }
            },
        });
    }

    this.componentName;
    this.data;

    this.rrr = function () { return _domMap; }

    this.component = function (name, options) {
        var collection = document.getElementsByTagName(name);

        if (collection.length) {
            _this_.componentName = name;
            _this_.data = options.data || {};
            _domMap[_this_.componentName] = [];
            prepareData(_this_.data);


            document.getElementsByTagName('input')[0].addEventListener('input', function () {
                _data.first = this.value;
            }, false);
            document.getElementsByTagName('input')[1].addEventListener('input', function () {
                _data.second = this.value;
            }, false);



            for (var i = 0, c = collection.length; i < c; i++) {
                var componentIndex = _domMap[_this_.componentName].push({ nodes: [], props: {} }) - 1;
                traverseNodes(collection[i], componentIndex);
                updateNodes();
            }
        }

        return _this_;
    };

    function traverseNodes(parent, componentIndex) {
        var collection = parent.childNodes;

        for (var i = 0, c = collection.length; i < c; i++) {
            if (collection[i] == parent) continue;

            if (collection[i].nodeType == Node.ELEMENT_NODE) {
                // element node
                traverseNodes(collection[i], componentIndex);
            } else if (collection[i].nodeType == Node.TEXT_NODE) {
                // text node
                var textSplit = collection[i].nodeValue.split(/({{.*?}})/);

                if (textSplit.length < 2) {
                    continue;
                }

                var nodeIndex = _domMap[_this_.componentName][componentIndex].nodes.push({
                    link: collection[i],
                    // props: [],
                    src: collection[i].nodeValue,
                    // srcArr: textSplit,
                    blocks: textSplit,
                }) - 1;

                for (var key in _data) {
                    if (collection[i].nodeValue.search(_data[key].regex) === -1) {
                        continue;
                    }

                    if (!_domMap[_this_.componentName][componentIndex].props[key]) {
                        _domMap[_this_.componentName][componentIndex].props[key] = {
                            nodeIndexes: [],
                        };
                    }

                    _domMap[_this_.componentName][componentIndex].props[key].nodeIndexes.push(nodeIndex);
                    // _domMap[_this_.componentName][componentIndex].nodes[nodeIndex].props.push(key);

                    _domMap[_this_.componentName][componentIndex].nodes[nodeIndex].blocks = _domMap[_this_.componentName][componentIndex].nodes[nodeIndex].blocks.map(function (val) {
                        return (_data[key].regex.test(val) ? { component: _this_.componentName, data: key } : val);
                    });

                    // console.log(_domMap[_this_.componentName][componentIndex]);
                }
            }
        }
    }

    function updateNodes() {
        for (var i = 0, c = _domMap[_this_.componentName].length; i < c; i++) {
            for (var ii = 0, cc = _domMap[_this_.componentName][i].nodes.length; ii < cc; ii++) {
                _domMap[_this_.componentName][i].nodes[ii].link.nodeValue = _domMap[_this_.componentName][i].nodes[ii].blocks.map(function (val) {
                    return ((typeof val === 'string' || val instanceof String) ? val : _data[val.data].value);
                }).join('');
            }
        }
    }

    return this;
};
