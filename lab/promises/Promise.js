function asyncFunction0(url) {
    if (('Promise' in window)) {
        console.log('Promise supports');

        return new Promise(function (resolve, reject) {
            setTimeout(function() { // for test async
                var xhr = new XMLHttpRequest();

                // for (var i = 0, c = 100000000; i < c; i++) {
                //     document.getElementsByTagName('*');
                // }

                xhr.open('get', url);
                xhr.onload = function () { resolve(xhr.responseText) };
                xhr.onerror = function () { reject(xhr.statusText) };
                xhr.send();
            }, 0); // for test async
        });
    } else {
        console.log('Promise not supports');

        setTimeout(function() {
            for (var i = 0, c = 100000000; i < c; i++) {
                document.getElementsByTagName('*');
            }

            console.log('I`m from settimeout')
        }, 0);
    }
}


function asyncFunction(executable) {
    if (!('Promise' in window)) {
        // console.log('Promise supports');
        return new Promise(executable);
    } else {
        // console.log('Promise not supports');

        var _this_ = this;
        var resolveFunc = [];
        var rejectFunc = [];
        var state = null;

        var resolve = function (resolveResult) {
            setTimeout(function () {
                while (resolveFunc.length) {
                    resolveResult = resolveFunc.shift().call(_this_, resolveResult);
                }
            }, 0);
        };

        var reject = function (rejectResult) {
            setTimeout(function () {
                while (rejectFunc.length) {
                    rejectResult = rejectFunc.call(_this_, rejectResult);
                }
            }, 0);
        };


        // this.state;

        // Object.defineProperty(this, 'state', {
        //     get: function () {
        //         return state;
        //     },
        //     set: function (newState) {
        //         state = newState;

        //         switch (newState) {
        //             case ('pending'): {
        //                 break;
        //             }
        //             case ('fulfilled'): {
        //                 break;
        //             }
        //             case ('rejected'): {
        //                 break;
        //             }
        //         }
        //     },
        // });


        this.then = function (_resolveFunc, _rejectFunc) {
            resolveFunc.push(_resolveFunc);
            rejectFunc.push(_rejectFunc);
            return _this_;
        };

        this.catch = function (_rejectFunc) {
            rejectFunc.push(_rejectFunc);
            return _this_;
        };

        this.all = function (iterable) {
            return _this_;
        }

        this.rise = function (iterable) {
            return _this_;
        }

        executable.call(_this_, resolve, reject);

        return this;
    }
}



console.log(123)

// asyncFunction0('test.json').then(function (res) {
//     res = JSON.parse(res);
//     console.log(res)
// });

// без then и без асинхронной функции внутри
asyncFunction(function () { console.log('Test 1.1'); })

// с then и без асинхронной функции внутри
asyncFunction(function (resolve) { console.log('Test 2.1');resolve('Test 2.2'); }).then(function (res) { console.log(res) });

// с then и с асинхронной функцией
asyncFunction(function (resolve) { setTimeout(function () { console.log('Test 3.1');resolve('Test 3.2'); }, 1000); }).then(function (res) { console.log(res);return 44444; }).then(function (res) { console.log(res) });

// с then, с асинхронной функцией и с reject
asyncFunction(function (resolve) {
    setTimeout(function () {
        console.log('Test 4.1');
        try {
            sasdas;
            resolve('Test 4.2');
            // throw('ooops');
        } catch (e) {
            resolve('Test 4.3 ' + e);
        }
    }, 1004);
}).then(function (res) { console.log(res) }, function (err) { console.log(err) });

// тест запроса с reject (локально не будет работать)
asyncFunction(function (resolve, reject) {
    setTimeout(function () {
        var xhr = new XMLHttpRequest();

        // "затратная процедура"
        // for (var i = 0, c = 100000000; i < c; i++) {
        //     document.getElementsByTagName('*');
        // }

        xhr.open('get', 'test.json'); // вернет json
        // xhr.open('get', 'test.php'); // вернет 404 ошибку
        xhr.send(null);
        xhr.onreadystatechange = function (aEvt) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200)
                    resolve(xhr.responseText);
                else
                    reject("Error loading page\n");
            }
        };
    }, 2000);
}).then(function (res) { console.log(JSON.parse(res)) }, function (err) { console.log(err) });


console.log(312)

// new Promise(function (resolve) {
asyncFunction(function (resolve) {
    setTimeout(resolve, 1000, 'sdfsdfiojo');
}).then(function (res) {
    console.log(res, 1);
    return res;
}).then(function (res) {
    console.log(res, 2);
    return res;
}).catch(function (err) {
    console.log(err)
    return err;
}).then(function (res) {
    console.log(res, 3);
    return res;
});
