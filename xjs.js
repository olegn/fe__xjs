var xjs = function (data) {
    // Коллекция элементов для подстановки значений из data
    var collection = document.getElementsByTagName('xjs:value-of');

    // Ссылка на себя
    var _this_ = this;

    // Одномерный объект с пользовательскими данными
    var dataMap = {};

    // Связанные объекты DOM с пользователскими данными
    var dataNodes = {};

    // В шаблоне можно вместо тэгов использовать интерполяцию {{data}}
    // поэтому, необходимо подменить ключевые слова на соответствующие тэги
    var bodyHTML = document.body.innerHTML;
    var regex = new RegExp('{{\\s{0,}(.*?)\\s{0,}}}', 'gm');
    bodyHTML = bodyHTML.replace(regex, '<xjs:value-of select="$1"></xjs:value-of>');
    document.body.innerHTML = bodyHTML;

    this.data = prepareDataObj(data)

    function prepareDataObj(data, path) {
        for (var key in data) {
            _path = path;
            _path = _path ? _path + '.' + key : key;
            dataMap[_path] = data[key];

            if (detectDataType(data[key]) == 'object') {
                data[key] = prepareDataObj(data[key], _path);
            } else if (detectDataType(data[key]) == 'array') {
                ObserveArray(data[key], true);
            } else {
                prepareDataProp(data, key, _path);
            }
        }

        return data;
    }

    function prepareDataProp(data, key, path) {
        Object.defineProperty(data, key, {
            get: function () {
                return dataMap[path];
            },
            set: function (newVal) {
                dataMap[path] = newVal;

                // console.log(newVal)

                updateNodes(path, newVal);
            },
        });
    }

    function updateNodes(path, newVal) {
        // for (var i = 0, c = collection.length; i < c; i++) {
        //     if (collection[i].getAttribute('select') == key) {
        //         collection[i].textContent = newVal;
        //         console.log(collection[i].textContent)
        //     }
        // }

        for (var i = 0, c = dataNodes[path].length; i < c; i++) {
            // if (window.event.target !== dataNodes[path][i]) {
            if (dataNodes[path][i].nodeValue != newVal) {
                dataNodes[path][i].nodeValue = newVal;
            }
        }
    }

    while (collection.length) {
        if (!collection[0]) continue;
        var path = collection[0].getAttribute('select');

        if (!dataNodes[path]) {
            dataNodes[path] = [];
        }

        var newTextNode = document.createTextNode(dataMap[path]);
        dataNodes[path].push(newTextNode);
        collection[0].parentNode.replaceChild(newTextNode, collection[0]);

        addEventToTextNode(newTextNode, path);
    }


    // test for-each
    var forEachColl = document.querySelector('[for-each]');
    var asd = forEachColl.getAttribute('for-each').match(/^(.*?)(?:\s{0,},\s{0,}(.*?))?\s+in\s+(.*?)$/i)
    console.log(asd)
    for (var i = 0, c = this.data[asd[3]].length; i < c; i++) {
        var _tfe = forEachColl.cloneNode(true);
        _tfe.textContent = this.data[asd[3]][i];
        _tfe.removeAttribute('for-each');
        console.log(_tfe)
        forEachColl.parentNode.insertBefore(_tfe, forEachColl.nextSibling);
    }
    forEachColl.parentNode.removeChild(forEachColl);


    function addEventToTextNode(textNode, dataPath) {
        textNode.addEventListener('DOMCharacterDataModified', function () {
            // console.log(this.nodeValue);
            // console.log(event, dataPath, dataMap[dataPath], getObjValFromStrKey(dataPath, _this_.data))
            // getObjValFromStrKey(dataPath, _this_.data)['two'] = 'asdadasdasdasd'

            setDataValByPath(dataPath, this.nodeValue);

            // if (_this_.data[dataPath] != this.nodeValue) {
            //     _this_.data[dataPath] = this.nodeValue;
            // }
        }, false);
    }

    function setDataValByPath(dataPath, value) {
        dataPath = dataPath.split('.');
        var dataObj = _this_.data;

        while (dataPath.length > 1) {
            dataObj = dataObj[dataPath.shift()];
        }

        dataObj[dataPath.pop()] = value;
    }

    function getObjValFromStrKey(key, obj) {
        key = key.split('.');

        while (key.length/*  && key[0] in obj */) {
            obj = obj[key.shift()];
        }

        return obj;
    }


    // test
    document.getElementsByTagName('input')[0].addEventListener('input', function () {
        _this_.data.first = this.value;
    }, false);
    document.getElementsByTagName('input')[1].addEventListener('input', function () {
        _this_.data.second.two = this.value;
    }, false);


    return this;
};
